## O que se passou?

### A preparação e o choque com as tecnológicas

No dia 1 de setembro de 2020, foi lançada publicamente a app Stayaway Covid, dedicada ao rastreio automático de contactos entre pessoas. A tecnologia era nova e por testar, mas um [consórcio europeu de investigação](https://github.com/DP-3T/documents) chegou a um protocolo respeitador da privacidade que passou a integrar as aplicações de rastreio de contactos (ARCs) pela Europa fora, incluindo Portugal.

Uma vez que este protocolo, baseado em Bluetooth, revelou provocar enormes perdas de bateria, a Google e a Apple juntaram-se ao esforço: enquanto responsáveis pelos dois sistemas operativos móveis mais usados (iOS e Android), propuseram-se a afinar a arquitetura dos sistemas que dão vida aos nossos telemóveis para que a operação do protocolo de rastreio fosse otimizada para poupar bateria.

No entanto, as duas gigantes também impuseram outra funcionalidade: não só
tornaram impossível que os dados que circulam pela app pudessem ser
desanonimizados e centralizados pelos Estados, como recusam qualquer contributo
ou imposição dos Estados na concepção e implementação desta funcionalidade nos
seus sistemas operativos móveis. Um grupo de Ministros e Secretários de Estado
de 5 Estados-Membros da UE, incluindo o Secretário de Estado para a Transição
Digital português, publicou uma [carta
aberta](https://www.linkedin.com/pulse/covid-19-aplica%C3%A7%C3%B5es-de-rastreamento-para-uma-sa%C3%ADda-europeia-azevedo)
a clamar por uma revisão dessa postura:

> O uso de tecnologias digitais deve ser projetado de uma forma que nós, como
> Governos eleitos democraticamente, as possamos avaliar e julgar como
> aceitáveis para os nossos cidadãos e compatíveis com os nossos valores
> europeus. Consideramos que a tentativa de questionar este direito, impondo
> normas técnicas, representa um passo em falso e uma oportunidade perdida de
> aprofundar e promover uma colaboração aberta entre governos e o setor privado.

As tecnológicas foram irredutíveis, e deixaram assim claro que o seu controlo
dos dispositivos significa que detêm de facto todo o poder de determinar o
formato de qualquer sistema que se quisesse implementar.

Em Portugal, a aplicação foi orientada por um consórcio liderado pelo [Inesc
Tec](https://inesctec.pt), aliado à [Keyruptive](https://keyruptive.com/) e
[Ubirider](https://ubirider.com), duas startups tecnológicas, com a assessoria
ética e de saúde por parte do Prof. Henrique Barros do [Instituto de Saúde
Pública da Univ. do Porto](https://ispup.up.pt/). A implementação do sistema de
códigos que forma o alicerce de todo o aparato que dá forma à Stayaway foi
levada a cabo pelos Serviços Partilhados do Ministério da Saúde.

### Os primeiros momentos

Embora a adesão inicial à app tenha sido notável, rapidamente surgiram os
primeiros números a indicar um baixo número de contactos positivos registados na
app.

No dia 14 de outubro, um mês e meio após o lançamento da Stayaway, o Governo
anunciou que o uso da app seria obrigatório em vários contextos, incluindo todas
as escolas e função pública. Houve reações imediatas a apontar a eventual
inconstitucionalidade da medida (um coro que incluiu o próprio [Presidente da
República](https://www.jornaldenegocios.pt/economia/coronavirus/detalhe/presidente-da-republica-admite-levar-obrigacao-da-app-stayawaycovid-ao-constitucional)),
a falta de clareza sobre a sua aplicação (estar sem bateria passaria a ser
ilícito?), e a existência de ramificações perigosas para a privacidade dos
cidadãos. Também as próprias forças da autoridade expressaram [fortes
reservas](https://www.dn.pt/pais/se-lei-passar-policias-podem-dar-voz-de-prisao-a-quem-recusar-mostrar-telemovel-12929883.html).

Seguiu-se um _blitz_ mediático provocado pelo assunto, com o tema a ser assunto
principal de colunas, comentários, debates, cartoons e _hashtags_ trocistas,
proporcionando uma irremediável politização da Stayaway, que veio colá-la a
debates periféricos e que contribuiu para a sua inclusão em discursos críticos
da ação do Governo na gestão da pandemia.

Graças à exposição mediática da Stayaway motivada por este episódio, o número de
downloads da aplicação disparou. Ao mesmo tempo, talvez devido à súbita adoção
da app por uma parte significativa da população, surgiram múltiplos relatos com
duas queixas centrais:

1. que não puderam obter o código para inserir na app junto do médico – não lhes
foi dado o código, mesmo quando especificamente pedido ao médico
2. que a app não havia denunciado após um contacto que as pessoas sabiam ter
ocorrido – por exemplo, quando uma pessoa mora com outra que introduziu o seu
código, e a app da primeira não "acende", embora os telemóveis tenham estado
próximos boa parte do tempo.

Boa parte das críticas dos proponentes da app foram direcionadas não só aos
serviços (SPMS) que implementaram o sistema de emissão de códigos, mas
particularmente [aos próprios médicos](https://www.pcguia.pt/2021/01/associacao-d3-acusa-o-inesc-tec-de-arrogancia-e-diz-que-app-stayaway-covid-e-inutil-no-confinamento/).

### A queda

Quando surgiu a terceira vaga em dezembro de 2020, o uso da app era diminuto,
longe até dos já baixos números dos primeiros dias de operação. Três meses
depois, em março, o Governo apresenta uma revisão da lei que regulamenta a
Stayaway. A CNPD apresentou pontos que necessitavam de definição antes de dar
parecer positivo à lei, e rapidamente a CNPD se viu como outro dos alvos de quem
insistia na eficácia da Stayaway – que já na altura estava moribunda, com
relatos de [grandes números de pessoas a
desinstalá-la](https://eco.sapo.pt/2021/01/15/60-dos-utilizadores-ja-apagaram-a-aplicacao-stayaway-covid/)
já desde janeiro.

Em maio, foi revelada a existência de uma [falha de segurança
grave](https://tek.sapo.pt/mobile/apps/artigos/d3-apela-a-suspensao-imediata-da-app-stayaway-covid-devido-as-falhas-de-seguranca-no-android)
nos dispositivos Android que colocou em risco os dados das pessoas, podendo ser
extraídos por parte de quem descobrisse e explorasse essa falha. Soube-se também
que a Google sabia da existência dessa falha desde fevereiro. Não se sabe se e
até que ponto essa falha foi explorada, mas esteve presente durante a maior
parte do ciclo de vida da Stayaway, desde o seu lançamento. O assunto teve
pouquíssima exposição mediática em Portugal; a Google nunca explicitou se sabia
de algum caso em que os dados das pessoas tenham sido acedidos indevidamente, e
não se ouviu falar de qualquer auditoria ou investigação por parte do Estado.

Uma das promessas iniciais foi a interoperabilidade com outras apps europeias,
um desígnio que silenciosamente foi colocado na gaveta pelos proponentes da
Stayaway à medida que o otimismo foi sendo refreado pelas evidências no terreno.
Minimizou-se a [grande
complexidade](https://www.politico.eu/article/google-apple-coronavirus-app-privacy-uk-france-germany/)
que tal interoperabilidade implicaria, como foi destacado num [estudo da
Université
Paris-Saclay](https://www.acm.org/binaries/content/assets/public-policy/europe-tpc-contact-tracing-statement.pdf).

O fim da terceira vaga veio provocar um alívio social que acalmou as polémicas
em volta da app, voltando a um certo silêncio mediático apenas pontuado pela
admissão que a app falhou, por parte do diretor do Inesc Tec, numa [entrevista ao
Expresso](https://expresso.pt/coronavirus/2021-04-23-Falhamos-todos-eu-falhei-a-minha-equipa-falhou-o-Ministerio-da-Saude-falhou-entrevista-ao-coordenador-da-app-StayAway-Covid-99a23218).
Mais tarde, foi ao ministro Manuel Heitor que coube fazer a [admissão oficial](https://eco.sapo.pt/2021/05/12/governo-admite-que-app-stayaway-covid-nao-funcionou/
) (mesmo que telegráfica e pouco densa) de que a app não havia funcionado.


### Visualização no tempo

Este gráfico representa a evolução dos códigos introduzidos (vermelho) ao longo
do tempo, com os novos casos diários (beige) como referência e os eventos-chave
que marcaram a evolução.

[![Linha temporal da Stayaway](/img/timeline.png)](/img/timeline.png)

É importante notar que a proporção de ambos não é a mesma, e a linha dos códigos
está multiplicada para melhor se perceber a evolução; por exemplo, no dia com
mais códigos introduzidos (5 de novembro), foram 69 os códigos introduzidos face
a 7947 casos de infeção nesse dia.

Estamos conscientes da armadilha de tirar demasiadas conclusões a partir de
números sem o devido contexto, mas apontamos algumas evidências que esta
visualização ajuda a compreender:

- A app foi lançada pouco antes do início da segunda vaga
- Ao longo da segunda vaga a adoção aumenta, sobretudo após o anúncio do Governo
  de que pretendia torná-la de uso obrigatório
- Ainda no pico da segunda vaga, o número de códigos introduzidos não mantém um
  crescimento sustentado
- À medida que a segunda vaga decai, também o número dos códigos introduzidos
  desce, mas após o início da terceira vaga no final de dezembro, não voltam a
  subir
- A app não teve expressão significativa durante toda a terceira vaga
- Em fevereiro encontramos o primeiro dia em que não houve qualquer código introduzido

Existem dois indicadores importantes que faltam neste gráfico:

- O **número de códigos** gerados pelo sistema de saúde, que tentámos obter junto dos SPMS mas não obtivemos resposta.
- O **número de aplicações ativas**, contabilizando o número de instalações e desinstalações; tentámos obter esses números junto do Inesc Tec, que nos indicou que não era possível partilhar esses dados.
