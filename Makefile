SSH_HOSTNAME = manufactura
SSH_DIR = /home/manufactura/apps/stayaway/
SSH_HOST = manufactura@opal5.opalstack.com
SSH_USER = manufactura

SSH_PATH = $(SSH_USER)@$(SSH_HOST):$(SSH_DIR)
BUILD_DIR = output
ACTIVATE=. `pwd`/.env/bin/activate

build: dir html

dir:
	mkdir -p $(BUILD_DIR)

html:
	$(ACTIVATE); PYTHONIOENCODING=utf-8 python3 generate.py
	cp -r css/ fonts/ img/ $(BUILD_DIR)
	# cp favicon.png $(BUILD_DIR)

serve:
	make -j2 runserver watch
runserver:
	cd $(BUILD_DIR); python3 -m http.server 8000
watch:
	watch make
clean:
	rm -fr $(BUILD_DIR)
install:
	python3 -m venv .env
	$(ACTIVATE); pip install -r requirements.txt

deploy: build
	rsync --compress --checksum --progress --recursive --update $(BUILD_DIR)/ $(SSH_PATH)
	ssh jplusplus -C "cd ~/repos/aid2growth; git pull"
dry-deploy: build
	rsync --dry-run --compress --checksum --progress --recursive --update $(BUILD_DIR)/ $(SSH_PATH)

.PHONY: clean install watch serve html css js dir fonts images data
