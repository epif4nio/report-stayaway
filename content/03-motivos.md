## O que correu mal?

### A app funcionava?
A Stayaway funcionou? A pergunta é ambígua: como é que determinamos se funcionou ou não? Qual é o critério para estipular que acima de X foi um sucesso?

Começou-se com um objetivo ambicioso: as apps só funcionariam com adesão de 60% da população. Foi um critério mais tarde clarificado: não seria fundamental uma taxa de adesão significativa para a app ser eficaz. Também era firme o objetivo das apps serem compatíveis entre países da UE, um objetivo que se ficou pelo papel. A adesão foi forte mas a eficácia prática foi residual. As promessas foram bastantes, e a realidade providenciou desilusões sucessivas que fizeram baixar mais e mais a fasquia do que significa a app "ter funcionado".

Mas voltamos à pergunta: a app funcionou? Aqui também encontramos uma confusão semântica, porque depende se estamos a falar:
- do sistema de rastreio digital de contactos, que é composto por uma app de telemóvel e pela funcionalidade de emitir códigos de ativação da app por parte de entidades de saúde
- ou apenas da app de telemóvel.

Recorrendo à analogia automóvel: quando ouvimos que a app funcionou, mas o sistema dos códigos não, é algo equivalente a dizer que a embraiagem e os travões do carro funcionam, mas o motor não. No final, o carro não funciona, e é pouco relevante salientar as componentes do carro que funcionam quando o bólide nem sequer liga. Com a app, mesmo aceitando o questionável argumento que a app Stayaway funcionava (já lá chegaremos), pouco consolo dá a quem esperava um sistema funcional.

Como tal, apenas com uma séria contorção se pode afirmar que a app funcionou, como fizeram ainda recentemente [investigadores](https://observador.pt/2021/07/01/condenar-a-aplicacao-stayaway-covid-foi-um-erro-que-esta-a-custar-caro/) e [deputados](/img/jose-magalhaes.png). O próprio ministro Manuel Heitor, na brevíssima menção que fez no rescaldo da experiência, afirmou que a app não funcionou. Encontramos, mesmo com a maior boa-vontade, muito pouca margem para considerar que a experiência Stayaway funcionou.

Também é discutível se a app Stayaway, por si, funcionava de facto.

### O público percebeu que a app não funcionava

A comunicação pública à volta da app minimizou as explicações sobre o seu
funcionamento técnico, e a estratégia de comunicações concentrou-se à volta de
um otimismo projetado à volta da Stayaway. Uma grande campanha publicitária foi
lançada na televisão, meios impressos e online, destacando a necessidade de
instalar a app e seguir as suas orientações. A direção de arte desses anúncios
era limpa e _glossy_, mostrando cidadãos aliviados rodeados de escudos
flutuantes. O slogan "Fique longe do covid com um clique" reforça a promessa de
que a app seria eficaz; sugere também que a app ajuda as pessoas a "ficar longe"
da doença, quando na verdade apenas serve para detetar (e não afastar) eventuais
contactos.

Além disso, declarações triunfantes a anunciar a futura adaptação do sistema
para outras doenças, ou usar a app para evitar quarentenas escolares de larga
escala, transmitiram uma convicção férrea na completa eficácia da app. Tal
otimismo manteve-se mesmo quando os números da app já eram mínimos, ao mesmo
tempo que eram publicadas [perspetivas
críticas](https://www.publico.pt/2020/12/26/opiniao/opiniao/empresas-farmaceuticas-6-empresas-tecnologicas-0-1944190)
por parte de quem inicialmente a defendeu, surgindo também análises a perguntar
[o que correu
mal](https://observador.pt/especiais/apenas-tres-mil-codigos-depois-o-que-aconteceu-a-app-de-rastreio-a-covid-19/).

Está agora claro que foi errada a estratégia de minimizar as referências às
limitações técnicas da app, que existem e eram bem conhecidas:

- a Stayaway e apps semelhantes [não funcionam dentro de
  elétricos](https://www.scss.tcd.ie/Doug.Leith/pubs/luas.pdf), metros e outras
  formas de transporte público
- é inevitável o surgimento de [muitos falsos positivos e
  negativos](https://arxiv.org/pdf/2011.04322.pdf), já que a app tem muitas
  dificuldades em funcionar entre multidões, e o próprio sinal Bluetooth penetra
  facilmente barreiras físicas e é facilmente bloqueado por certos materiais,
  como carteiras com elementos metálicos. O protocolo Bluetooth por si não é
  inteiramente fiável, tal como qualquer pessoa com uma coluna de som ou outro
  periférico Bluetooth sabe, pela experiência por vezes enfurecedora de
  estabelecer ligações com esses dispositivos.
- mesmo com uma utilização ideal, pode não haver deteção de contactos: manter um
  telemóvel "positivo" a centímetros de um "negativo" poderá não detetar a
  proximidade entre ambos, devido a fatores externos altamente variáveis, como a
  composição das paredes ou interferência de sinal provocada por outros
  aparelhos próximos.

Este último ponto foi claramente descoberto pelo público. Apareceram vários
relatos nas redes sociais com casos idênticos: um membro da família descobre que
está infetado e introduz o código dado pelo seu médico, mas os outros membros da
família (que sabem ter estado em contacto próximo com a pessoa infetada) não
vêem os seus aparelhos acender com um alerta de contacto. Entretanto, a campanha
mediática insistiu em apresentar um cenário positivo e idealista, passando
culpas de eventuais falhas para outros, em lugar de preparar as pessoas para uma
experiência por vezes imperfeita de uma app com claras e evidentes limitações.

### O escudo e a luz verde: comunicação, interface e transparência

Já abordámos como a comunicação à volta da app contribuiu para uma noção
irrealista da sua verdadeira eficácia. Mas além da projeção mediática,
encontramos outras evidências de uma comunicação excessivamente otimista no
próprio interface da aplicação.

No desenho da Stayaway, optou-se pela metáfora das cores do semáforo para
sinalizar os dois estados possíveis da aplicação: a cor verde para assinalar a
ausência de contacto detetado, e a cor amarela para o alerta de possível
contacto. O uso da cor verde, associada diretamente à segurança, poderia
comportar o risco de esse estado ser interpretado como estando tudo bem, embora
o máximo que a app possa assegurar seja a deteção de contactos entre
smartphones, e nada pode dizer sobre outros potenciais contactos ocorridos. Este
risco veio ser comprovado nas [declarações do próprio
Primeiro-Ministro](https://eco.sapo.pt/2020/09/18/antonio-costa-consulta-a-stayaway-todas-as-manhas-e-mesmo-preciso/)
em setembro de 2020: _"Eu uso [a Stayaway], e é com muita satisfação que, todos
os dias de manhã, tenho verificado até agora que ainda não estive próximo de
alguém que era um contacto de risco”_. Ora, aqui manifesta-se o equívoco de
confundir a luz verde da app com não ter tido qualquer contacto de risco – uma
conclusão que reforça uma falsa e perigosa ilusão de segurança.

O uso de um escudo como símbolo da app também merece referência. O escudo sugere
proteção pessoal, mas o princípio do rastreio de contactos é a proteção
comunitária. O rastreio serve para permitir a análise das cadeias de
transmissão, e trata-se de um mecanismo de deteção, não de proteção – esse é o
papel das vacinas e sistemas de saúde funcionais. É reforçada uma imagem de
segurança na medida que a app assume a forma de um objecto de defesa pessoal,
contribuindo para a imagem da app como um mecanismo de proteção que permite a
cada pessoa "ficar longe com um clique", que já articulámos como sendo
potenciador de mal-entendidos e, novamente, de falsas sensações de segurança. A
confusão é reforçada pelas palavras do Ministro da Ciência, Tecnologia e Ensino
Superior quando
[afirmou](https://expresso.pt/coronavirus/2020-09-01-App-StayAway-Covid-foi-apresentada-com-um-apelo-para-que-todos-a-utilizem-Descarregar-a-aplicacao-e-um-dever-civico-diz-Costa)
que "a aplicação não cura mas previne".

No anúncio, as pessoas surgem envolvidas por barreiras protetoras, num cenário
clínico e estéril. Uma analogia visual mais apta seria representar os telemóveis
das pessoas formando uma densa rede, traçando caminhos que depois são
"ativados", avisando as pessoas de potenciais contactos perigosos. Assim,
evitar-se-ia a associação confusa à proteção individual, retratar-se-ia mais
fielmente o processo de funcionamento do ecossistema de apps interconectadas, e
manter-se-ia o espaço para informar que por vezes o mecanismo de deteção pode
não funcionar 100% das vezes.

Tornou-se também confuso entender sob quem recaía a responsabilidade sobre a app
e quem a operava efetivamente – se o Inesc Tec, o Governo ou o Ministério da
Saúde. Muitos dos anúncios públicos relativos à app foram feitos por diretores
do Inesc Tec ou pelo próprio Primeiro-Ministro, em vez da Ministra da Saúde ou
da Diretora-Geral da Saúde, cujas declarações sobre o assunto se focaram na
divulgação de números e de incentivo à adoção. Esta confusão foi exacerbada pelo
anúncio da eventual obrigatoriedade da app. O Primeiro-Ministro foi o rosto
público desta medida, e os diretores do Inesc Tec foram rápidos a distanciar a
instituição, [descrevendo a
medida](https://eco.sapo.pt/2020/10/14/inesc-tec-surpreendido-com-intencao-do-governo-de-obrigar-uso-da-stayaway/)
como uma "decisão política" e declarando terem sido "apanhados de surpresa", com o
conselheiro de ética e saúde da Stayaway a [juntar-se aos
protestos](https://www.publico.pt/2020/10/14/sociedade/noticia/obrigar-usar-mascara-rua-instalar-app-sao-medidas-altamente-autoritarias-1935293).
Finalmente, na página da app na Google Play Store, encontramos uma conta chamada
"FCT FCCN" a responder oficialmente aos muitos comentários de utilizadores,
juntando-se à mistura de entidades com papéis públicos no projeto.

Todos estes elementos contribuem para a conclusão de que a comunicação da app
esteve empenhada somente em proporcionar uma fachada de auto-confiança e fé no
sucesso da aplicação, negligenciando princípios de transparência elementar e de
franqueza na altura de endereçar a eficácia do sistema. A título de exemplo, não
sabemos ainda o custo real desta experiência. Foram gastos 400 mil euros de
dinheiros públicos (providenciados pela FCT) no desenvolvimento da aplicação,
somados aos ainda não revelados custos de manutenção e de implementação do
sistema de emissão de códigos. Não temos acesso às fontes que permitiriam
estimar melhor quanto, realmente, a Stayaway nos custou em dinheiro e tempo. As
organizações que poderiam trazer alguma luz ainda não o fizeram.

### O jogo das culpas não serviu ninguém

Houve [queixas
persistentes](https://www.pcguia.pt/2021/01/associacao-d3-acusa-o-inesc-tec-de-arrogancia-e-diz-que-app-stayaway-covid-e-inutil-no-confinamento/)
por parte dos proponentes da app sobre a responsabilidade dos médicos pelo baixo
volume de códigos introduzidos na app; a CNPD também foi alvo de críticas por
supostamente estar a bloquear medidas para assegurar a eficácia da app. Até ao
momento, nenhuma parte assumiu responsabilidade explícita no insucesso da
aplicação. A admissão do governo [limitou-se a
reconhecer](https://eco.sapo.pt/2021/05/12/governo-admite-que-app-stayaway-covid-nao-funcionou/)
que a app não funcionou como planeado; o Inesc Tec admite também que a app
falhou, mas dilui a responsabilidade distribuindo-a por uma massa indefinida,
dizendo que "falhámos todos".

Quanto à responsabilidade dos médicos, parece-nos evidente que o tiro foi ao
lado, pois o aparente bloqueio veio de lacunas na implementação do sistema de
emissão dos códigos dentro do SNS. Eventualmente os proponentes da app refrearam
os ataques à classe médica, [apontando o
dedo](https://www.publico.pt/2021/01/15/tecnologia/noticia/60-ja-apagaram-stayaway-covid-sao-18-milhoes-portugueses-1946366)
à implementação do sistema de códigos. Tal, no entanto, não explica tudo nem
providencia um bode expiatório confortável. Contactámos médicos e associações do
setor para melhor compreender o que se passou, e os esclarecimentos que
obtivemos apontam para alguns fatores que foram pouco explorados no debate
mediático sobre o problema dos códigos:

- Outras tarefas tinham necessariamente maior prioridade, tal como o cuidado dos
  pacientes, supervisão e medicação, num contexto de [sobrecarga de
  esforços](https://expresso.pt/coronavirus/2020-11-13-Covid-19-Ordem-dos-Medicos-alerta-governo-para-problemas-graves-nos-centros-de-saude)
- Criou-se aversão à app na sequência do anúncio da eventual obrigatoriedade da
  Stayaway
- Não se promoveu adequadamente a app junto dos profissionais, sem apoio das
  associações médicas e uma fraca formação por parte do ministério, com alguns
  _webinars_ para lembrar os médicos que a app existia
- Ao contrário de outras funcionalidades da TraceCovid (o sistema geral de
  resposta à pandemia), a Stayaway não apresentava nenhum benefício operacional
  para o setor médico – tal como a possibilidade de automaticamente mapear
  cadeias de transmissão – o que a tornou uma prioridade menor face a outras
  tarefas e processos
- A existência de [falhas e
  indisponibilidades](https://www.publico.pt/2020/10/17/tecnologia/noticia/stayaway-covid-apos-38-mil-casos-gerados-apenas-730-codigos-infectados-1935607)
  do sistema, junto com insuficiências como a [incapacidade de consultar os
  códigos mais tarde](https://www.noticiasaominuto.com/pais/1606007/app-stayaway-covid-codigos-chegam-aos-doentes-com-atrasos-de-varios-dias) para pacientes que se haviam esquecido de os pedir na
  consulta.

Na [entrevista](https://expresso.pt/coronavirus/2021-04-23-Falhamos-todos-eu-falhei-a-minha-equipa-falhou-o-Ministerio-da-Saude-falhou-entrevista-ao-coordenador-da-app-StayAway-Covid-99a23218) onde afirmou que "falhámos todos", Rui Oliveira justificou o
fracasso com dois fatores que abordam o baixo número de códigos gerados:
uma mobilização insuficiente dentro do SNS à volta da app, e preocupações
excessivas com a privacidade, que terão posto em causa o envolvimento de
laboratórios privados na emissão de códigos, e impedido a geração automática de
códigos sem intervenção de um médico. Na mesma entrevista, Oliveira refere que
apenas um terço dos códigos emitidos pelos médicos foram efetivamente
introduzidos pelos pacientes – uma proporção que [os números
oficiais](https://tek.sapo.pt/mobile/apps/artigos/stayaway-covid-utilizadores-introduziram-menos-de-25-dos-codigos-gerados-pelos-medicos)
revelam ser de menos de um quarto. Uma vez que a crítica do Inesc Tec se foca na
geração dos códigos pelos médicos (e não na introdução pelas pessoas), a
baixíssima adesão por parte dos cidadãos fica por explicar.

Não encontrámos instâncias em que os proponentes da app tenham endereçado os
outros problemas encontrados – a falta de fiabilidade da própria app na altura
de detetar contactos, ou os diminutos números de códigos introduzidos. O Inesc
Tec assumiu um forte protagonismo mediático nas acusações que fez à classe
médica e à burocracia do SNS no falhanço da Stayaway, mas deixou completamente
de fora o óbvio impacto que a medida da obrigatoriedade por parte do governo
provocou na confiança das pessoas. Importa mencionar que o CEO da Ubirider,
parte do consórcio que desenvolveu a aplicação, classificou a medida do governo
como a ["machadada
final"](https://observador.pt/especiais/apenas-tres-mil-codigos-depois-o-que-aconteceu-a-app-de-rastreio-a-covid-19/)
no percurso da Stayaway, mas os representantes do Inesc Tec (que tiveram uma
presença mediática maior e mais persistente) nunca apontaram esse fator. O
Governo também não assumiu publicamente qualquer responsabilidade pelo efeito da
sua controversa medida.

Nenhuma análise póstuma da experiência Stayaway pode ignorar o papel das
limitações técnicas óbvias da app e da medida do governo de tornar a app
obrigatória, como causas significativas de uma crescente desconfiança na
aplicação, evidenciada pela baixa proporção de códigos introduzidos pelas
pessoas depois de os obterem, e também pelos números irrisórios de adoção ao
longo da terceira vaga de janeiro a março de 2021. Sem a confiança pública para
sustentar a adoção massiva, até uma app funcional encontraria o mesmo fracasso.


### A CNPD e as insuficiências do processo legislativo

A Stayaway exigia um enquadramento legal específico, e como tal foi aprovado em
Conselho de Ministros um decreto-lei para a enquadrar. Tal como qualquer
proposta que envolva tratamento dos dados pessoais dos cidadãos, foi chamada a
autoridade nacional de proteção de dados (CNPD) a dar o seu parecer. Note-se que o
parecer da CNPD foi pedido no próprio dia da aprovação na generalidade do
diploma, o que é irregular. Mesmo assim, o parecer limitou-se a apontar a
necessidade de esclarecimentos formais e concretização de ambiguidades
fundamentais motivadas por lacunas no texto original.

Meses mais tarde, a CNPD foi frequentemente alvo de críticas por ter impedido a
inclusão do setor privado e/ou profissionais de saúde que não médicos no
universo de entidades emissoras de códigos. No entanto, no [parecer da
CNPD](https://cnpd.pt/umbraco/surface/cnpdDecision/download/121776) encontramos
uma postura bem diferente da que foi denunciada:

> "...considera-se que a obtenção de um código de legitimação de diagnóstico
> (...) só poderá ser concretizada por um médico e não por outro profissional de
> saúde. Consequentemente, não se entende porque não fica esta questão definida
> na lei, desde já, em vez de deixar em aberto a possibilidade de o responsável
> pelo tratamento decidir noutro sentido."

E continua:

> "Mais, não é definido – como deveria ser no plano legislativo – qual o
> universo de profissionais de saúde (médicos) que se prevê que intervenham no
> sistema, se apenas os do setor público ou se também os do setor privado, o que
> poderá resultar num alcance bastante diferenciado da aplicação, comprometendo
> desde logo a sua finalidade e eficácia."

Ou seja, a própria CNPD destaca que não envolver o setor privado poderia ser
limitativo da eficácia do sistema, para mais tarde se ver acusada de ter
bloqueado o acesso aos privados – bloqueio esse que foi proporcionado apenas e
só pelo legislador que entendeu colocar essa restrição.

Mais tarde, depois de toda a polémica à volta da obrigatoriedade da app e da
constatação geral que havia problemas técnicos, começou a ser apontado
publicamente pelos proponentes da app que eram necessárias alterações
legislativas para permitir um alargamento da esfera de ação do sistema Stayaway.
É assinalável que a proposta de alteração legislativa para responder a essas
insuficiências só surge em março de 2021, mais de meio ano depois do projeto-lei
original, e meses depois da evidência que a Stayaway tinha problemas de
implementação e adesão. Na verdade, em março de 2021 o uso da aplicação já era
praticamente inexistente, quando em fevereiro já se havia registado vários dias
em que nenhum código foi introduzido pelas pessoas, sinalizando o abandono
generalizado.

Ou seja, a iniciativa de melhorar o enquadramento legal da Stayaway para
responder às preocupações dos proponentes da app foi tomada inexplicavelmente
tarde, quando já não poderia ter qualquer efeito. Vale a pena repetir a
visualização no tempo para perceber a magnitude do atraso:

[![Linha temporal da Stayaway](/img/timeline.png)](/img/timeline.png)

No entanto, não se viram críticas a esta lentidão, regressando as acusações à
CNPD por alegadamente ser a responsável pelo atraso na aprovação da alteração.
Efetivamente, a CNPD [voltou a
apontar](https://www.dn.pt/politica/governo-quer-alterar-aplicacao-stayaway-covid-13500625.html)
insuficiências na nova formulação do projeto-lei, sendo uma delas a indefinição
(novamente) da figura do "profissional de saúde", um reparo que já tinha sido
feito aquando do projeto-lei original e que inexplicavelmente volta a surgir –
não há aqui, claramente, mérito por parte do legislador em assegurar um processo
expedito de aprovação, ao repetir lacunas anteriores que inevitavelmente
resultariam na repetição da anterior crítica da CNPD.

Não encontrámos, na nossa extensa análise de imprensa, qualquer menção a este
grande atraso na legislação, ou sequer às insuficiências do projeto-lei e da
subsequente proposta de alteração, que poderiam ter sido evitadas com uma maior
diligência por parte do poder legislativo, bem como com uma melhor articulação
entre este e a CNPD. A clara disfuncionalidade do processo legislativo para
enquadrar uma nova medida de saúde pública mereceria um escrutínio mais próximo,
para melhor se saber reagir à próxima crise.


### O rastreio manual foi negligenciado

A Stayaway foi sempre apresentada como uma medida secundária para reforçar o
rastreio manual de contactos – o esforço de contactar as pessoas infetadas e
tentar determinar os possíveis contactos que tiveram recentemente, de forma a
mapear redes de transmisão. Tal caracterização foi regularmente mencionada em
resposta a críticas sobre lacunas e problemas na aplicação, defendendo-a apenas
como um meio secundário e não uma medida de saúde pública por si.

No entanto, desde o início da pandemia que o rastreio manual se caracterizou por
enormes défices de orçamento e recursos humanos: em novembro, durante a segunda
vaga, o Exército [juntou-se aos
esforços](https://www.publico.pt/2021/03/11/politica/noticia/militares-ja-fizeram-339-mil-contactos-rastreio-covid19-1953960)
de rastreio manual, junto com medidas de emergência para integrar [estudantes de
enfermagem](https://www.dn.pt/pais/dgs-admite-grande-pressao-na-saude-publica-alunos-de-enfermagem-vao-reforcar-equipas-12929098.html)
e [professores sem
horário](https://www.publico.pt/2020/11/28/sociedade/noticia/covid19-governo-convoca-professores-horario-combater-pandemia-1941044)
(e sem formação especializada na área da saúde). Houve momentos de desfalque
agudo, como a revelação de que apenas duas em cada dez pessoas estavam a ser
efetivamente acompanhadas pelo sistema de rastreio manual. Permanecerá a dúvida
se esta situação poderia ter sido aliviada se parte dos orçamentos disponíveis e
da atenção pública não estivessem desviados para uma medida de rastreio
secundária e experimental.

Esta situação é ainda mais incompreensível na medida que o rastreio manual e a
testagem massiva foram sempre apresentados (e reforçados por [evidência
académica](https://www.publico.pt/2020/07/17/ciencia/noticia/covid19-rapidez-testes-rastreio-contactos-mudam-rumo-infeccao-1924805))
como sendo os meios mais eficazes de mapeamento de cadeias e prevenção de
transmissão. A prioridade dada a uma aplicação experimental deve ser destacada
face a este contínuo défice de recursos para meios de eficácia comprovada,
défice que [ainda hoje
persiste](https://expresso.pt/opiniao/2021-07-16-Ao-setimo-dia-do-meu-isolamento-87b599f0).
