#!/usr/bin/env python3
import jinja2
import markdown
import os

script_path = os.path.dirname(os.path.realpath(__file__))

TEMPLATES_PATH = "templates/"
CONTENT_PATH = "content/sections/"


def render_template_into_file(env, templatename, filename, context=None):
    template = env.get_template(templatename)
    if not context:
        context = {}
    html = template.render(**context)
    with open(filename, "wb") as fh:
        fh.write(html.encode("utf-8"))


env = jinja2.Environment(
    loader=jinja2.FileSystemLoader([TEMPLATES_PATH]),
    extensions=["jinja2.ext.with_"],
    trim_blocks=True,
    lstrip_blocks=True,
)


def read_markdown_file(filename):
    print(filename)
    md = markdown.Markdown(extensions=["full_yaml_metadata", "tables", "toc"])
    content = open(filename, "r").read()
    html = md.convert(content)
    result = {}
    if md.Meta:
        result.update(md.Meta)
        # result['categories'] = result['taxonomy']['category']
        result["tags"] = result["taxonomy"]["tag"]
        result["original"] = open(filename, "r").read()
        del result["taxonomy"]
    # each meta field value is a list (to preserve linebreaks, see
    # https://python-markdown.github.io/extensions/meta_data/ )
    # we don't want this, so extract the single items
    result["content"] = html
    result["orig"] = content
    return result


def mkdir(dirpath):
    # creates a dir if it doesn't exist
    if not os.path.exists(dirpath):
        os.makedirs(dirpath)


def generate():
    md_intro = read_markdown_file("content/01-intro.md")
    md_eventos = read_markdown_file("content/02-eventos.md")
    md_motivos = read_markdown_file("content/03-motivos.md")
    md_conclusoes = read_markdown_file("content/04-conclusoes.md")
    # generate TOC from all the files
    full_md = (
        md_intro["orig"]
        + md_eventos["orig"]
        + md_motivos["orig"]
        + md_conclusoes["orig"]
    )
    print(full_md)
    tocmd = markdown.Markdown(extensions=["toc"])
    tocmd.convert(full_md)
    print(tocmd.toc)

    render_template_into_file(
        env,
        "index.html",
        "output/index.html",
        {
            "intro": md_intro["content"],
            "eventos": md_eventos["content"],
            "motivos": md_motivos["content"],
            "conclusoes": md_conclusoes["content"],
            "toc": tocmd.toc,
        },
    )


if __name__ == "__main__":
    generate()
