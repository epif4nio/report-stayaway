

> Muitos sistemas tecnológicos, olhando para o seu contexto e a sua arquitetura, são essencialmente anti-pessoas. As pessoas são vistas como fontes de problemas, enquanto que a tecnologia é vista como uma fonte de soluções.

> &mdash;Ursula M. Franklin, [The real world of technology](https://archive.org/details/realworldoftechn0000fran_g5p5/page/76/mode/2up), 1992

## Introdução

Este relatório é o resultado de um esforço levado a cabo pela
[D3](https://direitosdigitais.pt), a associação de defesa dos direitos digitais
em Portugal. A sua concretização foi possível graças a uma bolsa atribuída pela
[Liberties.eu](https://www.liberties.eu) para a elaboração de um relatório sobre
o tema a nível europeu, que tratámos seguidamente de adaptar e desenvolver para
o contexto nacional, tendo como resultado o presente testemunho.

Este é o fruto de centenas de horas investidas em pesquisa, debate interno e
conclusões encontradas ao longo de todo o tempo de vida da Stayaway, que optámos
por cristalizar num documento público para contrariar o preocupante silêncio que
se instalou à volta do tema, sem qualquer discussão pública subsequente à saída
de cena da aplicação.

Sentimos que é um debate fundamental para informar futuras
decisões e dilemas à volta do uso de tecnologias digitais experimentais como
mecanismos de saúde pública, e temos a esperança que a publicação deste relato
possa reavivar a consciência coletiva à volta das ramificações desta experiência
bio-tecno-política. Aguardámos que surgisse algo nesse sentido por parte das
entidades que estiveram mais próximas do processo mas, na ausência disso,
sentimos o dever de avançar com o debate desta forma.

Esforçámo-nos para concretizar uma análise séria dos eventos e suas
consequências, abstendo-nos do slogan pomposo, da indignação juvenil ou de
justificações conspiratórias. Não estamos na posse de todos os dados, e
prometemos o esforço de identificar ambiguidades sempre que delas estivermos
conscientes.

Este documento reparte-se em três secções:

- **O que se passou**, onde é recordada a história da Stayaway desde o seu
  lançamento, os episódios-chave que marcaram o seu ciclo de vida até ao seu
  final. Nesta secção, incluímos também uma análise de dados, em forma de
  _timeline_, a ilustrar visualmente a evolução da aplicação.
- **O que correu mal**, onde são desenvolvidas constatações e análises de várias
  facetas do assunto, de forma a levantar o véu sobre pormenores importantes que
  ficaram por discutir.
- **Como fazer melhor da próxima?**, onde expomos as conclusões a que chegamos
  face ao exposto nos dois pontos anteriores, concretizando um contributo para o
  necessário debate sobre o que foi a experiência Stayaway, para melhor informar
  as abordagens a tecnologias experimentais nas próximas crises.
