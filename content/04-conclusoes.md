## Como fazer melhor da próxima?

> A noção de que talvez a tecnologia constitua uma fonte de problemas e
> injustiças, e que as pessoas possam ser vistas como uma fonte de soluções,
> muito raramente entrou nas políticas públicas, ou sequer na consciência
> pública.
>
> &mdash;Ursula M. Franklin, [The real world of technology](https://archive.org/details/realworldoftechn0000fran_g5p5/page/76/mode/2up), 1992

### Não funcionou e teve consequências

Como articulámos antes, mesmo com a maior generosidade temos de concluir que a
experiência Stayaway não funcionou.

Durante esta experiência, o rastreio manual – a medida de combate à pandemia que
a app supostamente iria apenas complementar – teve falhas gravíssimas. As
notícias sobre estes problemas ficaram sempre em segundo plano face ao
mediatismo da Stayaway, e a campanha publicitária da app foi muito mais visível
do que qualquer esforço de informação pública sobre a importância do rastreio
manual.

Uma consequência clara foi um desgaste da confiança da população em apps de
saúde pública, algo que vai complicar esforços futuros. Outras apps futuras,
mesmo que funcionais e promissoras, passarão a ser alvo de desconfiança à
partida por parte de quem viu as suas esperanças na Stayaway desaparecerem numa
notificação não recebida.

Outro efeito negativo, difícil de quantificar, é o desgaste provocado junto da
classe médica, que se viu alvo de culpas do fracasso de um sistema que não foi
devidamente desenhado para corresponder às necessidades no terreno. Chegar a
casa, depois de turnos contínuos a segurar a unidade de cuidados intensivos, e
deparar-se com declarações acusatórias – não é a forma de tratar os
profissionais envolvidos ativamente no esforço de resposta à crise pandémica.
Acrescentemos também o igualmente inquantificável stress causado a quem recebeu
notificações erróneas e seguiu os conselhos da app para se isolar
desnecessariamente.

E mesmo que a app funcionasse? É aceitável, em nome de uma tecnologia
experimental e de eficácia não comprovada, apressar parcerias de saúde pública
com grandes empresas tecnológicas que recebem dados de saúde identificáveis, sem
qualquer auditoria ou supervisão? Mesmo quando se tornou claro que nem elas
podem garantir que os nossos dados permanecem seguros, como demonstrado pela
falha de segurança revelada em maio de 2021?


### O fracasso do tecno-deslumbramento

Concluímos que a Stayaway foi apresentada como um recurso suplementar que se
pode lançar sem grandes receios, que no pior dos casos simplesmente não terá
efeitos negativos; esta postura está patente na repetição, nas mais altas
instâncias, que "se a app salvar uma vida, já valeu a pena". Em alternativa,
defendemos que a aplicação devia ter sido tratada como um recurso de potencial
eficácia, mas que pode comportar efeitos prejudiciais se não devidamente
estudada e testada, e como tal requer particulares cuidados na sua análise e
estratégia de adoção.

Embora conscientes do pânico e instabilidade vividos na altura de considerar as
possíveis medidas de combate à pandemia – recordemos que começou a ser
equacionada um mês depois do início da primeira vaga em Portugal –, tivemos o
cuidado de não tomar como premissa os factos que apenas conhecemos hoje como
devendo ter sido considerados na altura. No entanto, meses antes do lançamento
da app já existiam estudos e perspetivas que punham em questão o otimismo e fé
nesta solução tecnológica e moderna baseada em Bluetooth.

Apelidamos de "tecno-deslumbramento" a doutrina que exalta a supremacia de
"soluções" digitais inovadoras, contemplável apenas segundo a sua dimensão
técnica, desprezando qualquer influência de outra ordem que não a técnica,
encarando os direitos humanos e a legislação como indesejáveis obstáculos que
devem ser ultrapassados em nome da inovação. O mantra de Silicon Valley "Move
fast and break things" ajuda a caracterizar essa postura de implementação rápida
de "soluções" inovadoras, mesmo consciente de que pode haver riscos e efeitos
negativos dessa corrida para pôr novas tecnologias no terreno.

Como constatamos agora, tal princípio não se traduz nada bem para iniciativas de
saúde pública, que exigem uma análise e debate bem mais profundos do que o
modelo de negócio de uma startup. Reconhecemos no discurso oficial de apologia
da app, com a ênfase na inovação, tecnologia de ponta e engenharia de
excelência, uma manifestação evidente desta doutrina, e afirmamos a
insuficiência da postura tecno-deslumbrada em responder eficazmente a contextos
sociais que extravasam a simples engenharia.

A arrogância do sector tecno-deslumbrado também está visível na primazia da
engenharia em detrimento de qualquer outra área de estudo, como as ciências
sociais. Vários dos problemas que foram constatados no terreno, apresentados
como imprevisíveis pelos proponentes da Stayaway, poderiam ter sido facilmente
identificados se a equipa de desenvolvimento pudesse contar com o contributo de
especialistas em sociologia (para compreender a recepção pública), direito e
ciência política (para formular devidamente a legislação necessária), proteção
de dados (para antecipar potenciais problemas que possam causar atrasos na
implementação) ou design de interfaces (para identificar as metáforas de
comunicação adequadas). Não deixamos de expressar algum estarrecimento ao ler,
recentemente, académicos e deputados a absolver a aplicação de qualquer defeito,
apontando o dedo à opinião pública, defensores da privacidade ou (mesmo agora)
aos médicos, validando a perspetiva tecno-deslumbrada mesmo quando esta não tem
qualquer vitória para apresentar depois da experiência.

Agora que estamos na fase de tirar conclusões, torna-se claro que as novas
tecnologias, sobretudo as de carácter experimental, devem ser profundamente
escrutinadas e encaradas com reserva face às promessas exaltadas de quem as
avança. Encontramos outra evidência desta necessidade de algum ceticismo numa
análise recente do MIT que demonstra a [ineficácia de todas as novas soluções
baseadas em inteligência
artificial](https://www.technologyreview.com/2021/07/30/1030329/machine-learning-ai-failed-covid-hospital-diagnosis-pandemic/)
para abordar a pandemia. Não deve haver lugar para o tecno-deslumbramento na
formulação de políticas de saúde pública.


### De quem é a responsabilidade?

Dentro de qualquer noção de responsabilidade democrática, é imperativo
identificar o que correu mal e a cadeia de responsabilidades que levou a tal
desfecho. A publicação deste relatório responde à clara insuficiência, por parte
das entidades envolvidas, em realizar qualquer tipo de esclarecimento pós-facto.

Na sequência do que articulámos na secção anterior, identificamos
responsabilidades evidentes por parte de algumas entidades no cenário que agora
constatamos:

- o poder legislativo e o Governo, pela lentidão na adequação das suas propostas
  às necessidades reais, pela persistente hostilização da CNPD como entrave a um
  trabalho que, como expusemos, podia ter sido muito melhor executado, e pela
  desnecessária politização da app que se revelou um fator incontornável no seu
  insucesso
- os proponentes da app, pela desnecessária comunicação pública centrada no
  desvio de responsabilidades e acusação de terceiros (dedicando particular
  atenção aos médicos), e pela falta de clareza e transparência no
  desenvolvimento e desenrolar da aplicação
- toda a estrutura responsável pelo sistema Stayaway (tanto a app como o sistema
  de códigos), pelas claras falhas em gestão de projeto, trabalho em conjunto e
  incapacidade de implementar um sistema eficaz em tempo útil

Existiu um grave défice de transparência. Em algum momento o Governo ou os
promotores da Stayaway veicularam que o grau de fiabilidade da app era
impreciso; algo que já tinha sido sugerido por estudos prévios ao lançamento (e
confirmado por estudos posteriores), e a baixa eficácia da app foi facilmente
comprovada pelo público ao não receber notificações quando sabia que deveria. Se
tivesse existido outro grau de modéstia na comunicação, repensando a sua
caracterização mediática como um imaculado escudo flutuante, e um pouco menos de
destaque a slogans e mais à informação, talvez a confiança nesta app (e em apps
futuras) não tivesse sido posta em causa da forma como foi.

A CNPD é das poucas entidades que pode reclamar mérito em todo este episódio,
não só pela postura vigilante e pró-ativa face aos dilemas que a nova legislação
trouxe, como soube identificar (sob críticas) os riscos de depender das grandes
tecnológicas para implementar esta medida de saúde pública. A grave falha de
segurança no Android revelada em maio (e cuja existência a Google já conhecia
desde fevereiro, mas não transmitiu) veio demonstrar como a CNPD tinha razão ao
apontar esse risco ainda antes do lançamento da app, e como é merecedora da
confiança que não teve e que foi, em vez disso, concedida às grandes
tecnológicas que não souberam estar à altura da responsabilidade de serem parte
de um esforço de saúde pública. A aparente barreira e hostilidade expressa face
à CNPD por parte do poder legislativo parece ser também um obstáculo que
permanecerá na altura de reagir rapidamente à próxima crise.


### Notas finais

Esperamos que este documento possa ressuscitar o necessário debate sobre
mecanismos digitais de saúde pública, até onde podemos esticar os direitos das
pessoas, e em nome de quê será legítimo questionar esses direitos. Ainda hoje
encontramos anúncios nos transportes públicos a instalar a aplicação, bem como
posturas revisionistas que culpam a "opinião pública" pelo insucesso desta
experiência. Temos a esperança que este documento possa ajudar a re-erguer a
necessária seriedade no discurso que, como tristemente constatamos, também
parece ter sido posta de parte por quem tem a obrigação de abordar o assunto de
forma séria.

Os autores deste relatório também partilham da ansiedade que a pandemia veio
provocar, e também mantêm o desejo honesto de que haja contributos para o seu
combate na forma de apps ou outras soluções digitais -- é sem cinismo que
afirmamos que gostávamos que a aplicação tivesse funcionado, e que esperamos
que novas tentativas possam ter melhores desfechos.

Mas, para isso, é urgente elevar o debate sobre o digital, compreendendo a forma
como molda e afecta a nossa estrutura social e a nossa integridade mais íntima,
para evitar ingenuidades que motivaram boa parte dos erros identificados em todo
este processo, e que ainda alimentam análises irresponsavelmente superficiais
por parte de quem tem obrigação de fazer melhor. Uma abordagem mais sensata e
experiente saberia também evitar os problemas que já surgiram com o certificado
digital.

Com a publicação deste relatório, esperamos que outras análises possam surgir e
complementar a necessária compreensão sobre como poderemos fazer melhor da
próxima, enquanto cidadãos que acreditam -- mas não cegamente -- no potencial do
digital.


## Cólofon

- **Coordenação, edição e dados**: Ricardo Lafuente
- **Website**: Ana Isabel Carvalho
- **Contributos**: Ana Aguiar, Eduardo Santos, Hugo Peixoto, Marcos Marado, Paula Simões, Tiago Epifânio

Este projeto foi apoiado pelo European AI Fund, uma iniciativa conjunta da
Network of European Foundations (NEF). A responsabilidade pelo projeto recai sob
os organizadores e o seu conteúdo poderá não refletir as posições do European AI
Fund, NEF ou as Fundações Parceiras do European AI Fund.

## Agradecimentos

- à Liberties.eu e ao European AI Fund, pela atribuição de uma bolsa para
  desenvolver um capítulo sobre Portugal para um relatório sobre as ARCs na
  Europa, que serviu de base para este documento.
- a todos os membros e simpatizantes da D3 que, num momento ou noutro,
  participaram no animado debate interno que mantivemos durante meses à medida
  que tentávamos compreender toda a situação e formular argumentários à volta
  dela
- ao Rui Cavaco, por nos ter apontado os datasets do Trinity College Dublin
- ao Inesc Tec, por ter facultado os datasets dos códigos introduzidos e número
  de downloads
